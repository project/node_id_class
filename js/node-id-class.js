/**
 * @file
 * Show summaries of selected options within tabs.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.nodeTypeClass = {
    attach: function (context) {

      // Display save draft settings summary on the node options fieldet.
      $('#edit-node-id-class-fieldset', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-body-id', context).val() !== '') {
          vals.push(Drupal.checkPlain($('#edit-body-id', context).val()));
        }

        if ($('#edit-body-class', context).val() !== '') {
          vals.push(Drupal.checkPlain($('#edit-body-class', context).val()));
        }

        if ($('#edit-node-class', context).val() !== '') {
          vals.push(Drupal.checkPlain($('#edit-node-class', context).val()));
        }
        if(vals.length > 0) {
          return vals.join(', ');
        }
      });
    }
  };

})(jQuery);
