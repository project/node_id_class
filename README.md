# OVERVIEW
 * About
 * Requirements
 * Installation
 * Configuration

## ABOUT THIS MODULE
The 'Node ID Class' module enables users to customize content types by
adding custom IDs and CSS classes. This customization includes the ability
to assign CSS body IDs and classes, as well as node-specific classes.
Users can utilize dynamic tokens such as {node_id} for the node's ID,
{node_title} for the node's title, and {bundle} for the content type's bundle.

## REQUIREMENTS
This module does not depend on any other modules for its functionality.

## INSTALLATION
 * Commerce stripe checkout, like all Commerce modules, must be installed
 via Composer.
 `composer require drupal/node_id_class`
 * Or install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules


## CONFIGURATION
This module introduces a fieldset titled 'Node ID Class' within the node
type form, accessible at locations such as
'admin/structure/types/manage/article' for the 'Article' content type.
